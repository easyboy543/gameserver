package ncu.cise.game.tcp;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ncu.csie.game.ClientManager;
import ncu.csie.game.udp.UDPClient;

public class TCPServer
{
    public static final int LISTEN_PORT = 9999;       
    private ClientManager clientManager = new ClientManager();
    
    public void listenRequest()
    {
    	
        ServerSocket serverSocket = null;
        ExecutorService threadExecutor = Executors.newCachedThreadPool();
        try
        {
            serverSocket = new ServerSocket( LISTEN_PORT );
            System.out.println("Server listening requests...");
            while ( true )
            {
                Socket socket = serverSocket.accept();
                threadExecutor.execute( new RequestThread( socket ) );
            }
        }
        catch ( IOException e )
        {
            e.printStackTrace();
        }
        finally
        {
            if ( threadExecutor != null )
                threadExecutor.shutdown();
            if ( serverSocket != null )
                try
                {
                    serverSocket.close();
                }
                catch ( IOException e )
                {
                    e.printStackTrace();
                }
        }
    }       
    
    /**
     * @param args
     */
    public static void main( String[] args )
    {
    	TCPServer server = new TCPServer();    	
		server.listenRequest();		
    }
    
    
    
    
    /**
     * 處理Client端的Request執行續。
     *
     * @version
     */
    class RequestThread implements Runnable
    {
        private Socket clientSocket;
        
        public RequestThread( Socket clientSocket )
        {
            this.clientSocket = clientSocket;
        }
        
        /* (non-Javadoc)
         * @see java.lang.Runnable#run()
         */
        @Override
        public void run()
        {
        	String clientIP = clientSocket.getInetAddress().getHostName();
            System.out.printf("有%s連線進來!\n", clientSocket.getRemoteSocketAddress() );
            DataInputStream input = null;
            DataOutputStream output = null;
            
            try
            {
                input = new DataInputStream( this.clientSocket.getInputStream() );
                output = new DataOutputStream( this.clientSocket.getOutputStream() );
                UDPClient client = new UDPClient(clientIP, 8888);
                while ( true )
                {
                    output.writeUTF( String.format("Hi, %s!\n", clientSocket.getRemoteSocketAddress() ) );
                    output.flush();
                    // TODO 處理IO，這邊定義protocol協定！！
                    String clientInput = input.readUTF();
                    if(clientInput.startsWith("#")){
                    	clientManager.addClientToRoom(clientInput.replace("#", ""), clientIP);
                    	//UDP
                    	Timer timer = new Timer();
                    	while(true){
	                    	timer.schedule(new TimerTask() {
									
								@Override
								public void run() {									
									client.sendData(clientManager.getWaitForLinkingCount(clientIP)+"");									
								}
							}, 500);
	                    	
                    	}                    	                    	                    	
                    }                   
                    else if(clientInput.startsWith("@")){
                    	
                    	
                    }
                    
                    if(clientManager.getWaitForLinkingCount(clientIP) == 0){
                    	String roomName = clientManager.getRoomNameFromIP(clientIP);
                    	clientManager.initGame(roomName);
                    }
                    
                    break;
                }
            }
            catch ( IOException e )
            {
                e.printStackTrace();
            }
            finally 
            {
                try
                {
                    if ( input != null )
                        input.close();
                    if ( output != null )
                        output.close();
                    if ( this.clientSocket != null && !this.clientSocket.isClosed() )
                        this.clientSocket.close();
                }
                catch ( IOException e )
                {
                    e.printStackTrace();
                }
            }
        }
    }
}
