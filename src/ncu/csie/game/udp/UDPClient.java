package ncu.csie.game.udp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UDPClient {

	private String serverIP;
	private int serverPort;
	private DatagramSocket clientSocket;
	private InetAddress IPAddress;

	public UDPClient(String serverIP, int serverPort) {
		this.serverIP = serverIP;
		this.serverPort = serverPort;
	}

	public void sendData(String clientData) {
		try {
			
			clientSocket = new DatagramSocket();			
			IPAddress = InetAddress.getByName(this.serverIP);
			byte[] sendData = clientData.getBytes();
			DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, this.serverPort);
			clientSocket.send(sendPacket);												
			clientSocket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/*
	 * public static void main(String[] args) throws Exception{ BufferedReader
	 * inFromUser = new BufferedReader(new InputStreamReader(System.in));
	 * DatagramSocket clientSocket = new DatagramSocket(); InetAddress IPAddress
	 * = InetAddress.getByName("127.0.0.1");
	 * 
	 * byte[] sendData = new byte[1024]; byte[] receiveData = new byte[1024];
	 * 
	 * String sentence = inFromUser.readLine(); sendData = sentence.getBytes();
	 * 
	 * DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length,
	 * IPAddress, 9876); clientSocket.send(sendPacket); DatagramPacket
	 * receivePacket = new DatagramPacket(receiveData, receiveData.length);
	 * clientSocket.receive(receivePacket);
	 * 
	 * String modifiedSetence = new String(receivePacket.getData());
	 * System.out.println("FROM SERVER: "+modifiedSetence);
	 * clientSocket.close(); }
	 */
}
