package ncu.csie.game;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import ncu.csie.game.worlds.World;

public class ClientManager {
	
	private final int MAXROOMNUM = 1;
	private TreeMap<String, List<String>> room;
	private TreeMap<String, Handler> roomGame;
	private String mapFilePath = "/worlds/world2.txt";
	
	public ClientManager(){
		room = new TreeMap<>();
		roomGame = new TreeMap<>();
	}
	
	public void addClientToRoom(String roomName, String ip){
		if(!room.containsKey(roomName)){
			List<String> client_ip = new ArrayList<>();
			client_ip.add(ip);
			room.put(roomName, client_ip);
		}
		else{
			if(room.get(roomName).size() < MAXROOMNUM)
				room.get(roomName).add(ip);
		}
	}
	
	public int getWaitForLinkingCount(String ip){
				
		for(List<String> ip_box: room.values()){
			if(ip_box.contains(ip)){
				return MAXROOMNUM-ip_box.size();
			}
		}
		return -1;
	}
	
	public void initGame(String roomName){
		Handler handler = new Handler();
		World world = new World(handler, mapFilePath);
		handler.setWorld(world);
		if(!roomGame.containsKey(roomName)){
			roomGame.put(roomName, handler);
		}		
	}
	
	public String getRoomNameFromIP(String ip){
		for(String roomName: room.keySet()){
			List<String> ipbox = room.get(roomName);
			if(ipbox.contains(ip)){
				return roomName;
			}
		}
		throw new IllegalStateException("there is no ip in the current room.");
		
	}
	
	public Handler getGameFromRoomName(String roomName){
		if(roomGame.containsKey(roomName)){
			return roomGame.get(roomName);
		}
		throw new IllegalStateException("there is no game in the current room.");
	}
	
}
