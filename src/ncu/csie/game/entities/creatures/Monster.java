package ncu.csie.game.entities.creatures;

public class Monster {
	private int speedup=0;
	private int speed = -1;//run speed
	private int ID = -1;
	private int attack = -1;//attack power
	private int target = -1;
	private int losehp = 0;
	private int direction = -1;
	//
	public Monster(int MonsterID ,int Speed ,int Attack,int Hp)
	{
		ID = MonsterID;
		speed = Speed;
		attack = Attack;
		losehp = Hp;
	}
	//
	public int GetID()
	{
		return ID;
	}
		
	public int GetSpeed()
	{
		return speed;
	}
	public void SetSpeed(int sp)
	{
		speed = sp;
	}
	
	public int GetDirection()
	{
		return direction;
	}
	public void SetDirection(int dr)
	{
		direction = dr;
	}
	
	public int GetLoseHp()
	{
		return losehp;
	}
	
	public int GetAttackspeed()
	{
		return attack;
	}
	
	public int GetTarget()
	{
		return target;
	}
	public void SetTarget(int PlayerID)
	{
		target = PlayerID;
	}
	
/*	public int GetAttackNextDri()
	{
		return atnextdir;
	}
	public void SetAttackNextDri(int dri)
	{
		atnextdir = dri;
	}*/
	
	public void SetAttack(int at)
	{
		attack = at;
	}
	public void setSpeedup(int sp)
	{
		speedup= sp;
	}
	
	public int getSpeedup()
	{
		return speedup;

	}
	
}